(function() {
'use strict';

var app = angular.module("myApp", ['ngAnimate']);

app.run([
	'$rootScope',
	'$window',
	function($rootScope, $window) {

		// データの取得
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: 'data/data.json',
			async: false,
			success: function(data) {
				$rootScope.data = data;
			},
			error: function(){
				console.log('error');
				$rootScope.data = [];
			}
		});

		function _initRootScopeArg() {
			// スライド切り替え用フラグ
			$rootScope.main = true;
			$rootScope.menu = false;
			$rootScope.edit = false;

			$rootScope.stores = [];

			// お店の選択情報
			$rootScope.currentStore = {
				select : false,
				index : undefined
			};
			// フォルダの選択情報
			$rootScope.currentFolder = {
				select : false,
				index : undefined
			};
		}

		_initRootScopeArg();
	}
]);

app.controller('mainCtrl', [
	'$rootScope',
	'$scope', 
	function($rootScope, $scope) {
		// rootScopeのデータを参照する
		$scope.currentStore = $rootScope.currentStore;
		$scope.currentFolder = $rootScope.currentFolder;
		
		$scope.toggleMenu = function() {
			$rootScope.menu = !$rootScope.menu;
			$rootScope.main = !$rootScope.main;
		};

		$scope.toggleEdit = function() {
			$rootScope.edit = !$rootScope.edit;
			$rootScope.main = !$rootScope.main;
		};

		$scope.isClicked = function(storeSelect, index) {
			if(storeSelect && $scope.currentStore.index==index) {
				return true;
			} else {
				return false;
			}
		};
		$scope.toggleStoreDetail = function(index, storeName) {
			if($scope.currentStore.index == index) {
				$scope.currentStore.select = !$scope.currentStore.select;	
			} else {
				$scope.currentStore.index = index;
				$scope.currentStore.select = true;
			}

			if($scope.currentStore.select) {
				$('head script').remove();
				// googleAPIにアクセス
				var script = document.createElement('script');
				script.src = "http://ajax.googleapis.com/ajax/services/search/images?callback=gglApiCallback&q="+storeName+"&v=1.0";
				document.head.appendChild(script);
			}
		};

		$scope.link = function(store) {
			var w = window.open();
			w.location.href = "http://www.google.com/search?q=" + store.name + "+" + store.tags[0] + "+" + store.tags[1];
		};
	}
]);

app.controller('menuCtrl', [
	'$rootScope',
	'$scope', 
	function($rootScope, $scope) {
		// rootScopeのデータを参照する
		$scope.currentStore = $rootScope.currentStore;
		$scope.currentFolder = $rootScope.currentFolder;
		$scope.folders = $rootScope.data.folders;

		$scope.toggleFolder = function(index) {
			if($scope.currentFolder.index == index) {
				$scope.currentFolder.select = !$scope.currentFolder.select;		
			} else {
				$scope.currentFolder.select = true;
			}
			$scope.currentFolder.index = index;
		};
		$scope.isOpenedFolder = function(index, folderSelect) {
			if(folderSelect && $scope.currentFolder.index==index) {
				return true;
			} else {
				return false;
			}
		};
		
		$scope.searchStores = function(keyword, event) {
			$scope.currentStore.select = false;

			$('#menu li.active').removeClass('active');
			$rootScope.stores = [];
			for(var i=0; i<$rootScope.data.stores.length; i++) {
				var tags = $rootScope.data.stores[i].tags;
				for(var j=0; j<tags.length; j++){
					if(tags[j] === keyword) {
						$rootScope.stores.push($rootScope.data.stores[i]);
					}	
				}
			}
			$(event.currentTarget).addClass('active');
		};
	}
]);

app.controller('editCtrl', [
	'$rootScope',
	'$scope', 
	function($rootScope, $scope) {
		$scope.currentStore = $rootScope.currentStore;
		$scope.currentFolder = $rootScope.currentFolder;

		// inputのモデル
		$scope.store = {
			name: "",
			place: "",
			dish: ""
		};

		$scope.isFilledText = function(store) {
			if(store.name && store.place && store.dish) {
				return true;
			} else {
				return false;
			}
		};

		$scope.addStore = function() {
			var dishHit = 0;
			for(var i=0; i<$rootScope.data.folders[0].list.length; i++) {
				if($rootScope.data.folders[0].list[i] === $scope.store.dish) {
					dishHit++;
				}
			}

			if(!dishHit) {
				$rootScope.data.folders[0].list.push($scope.store.dish);
			}

			var placeHit = 0;
			for(var i=0; i<$rootScope.data.folders[1].list.length; i++) {
				if($rootScope.data.folders[1].list[i] === $scope.store.place) {
					placeHit++;
				}
			}

			if(!placeHit) {
				$rootScope.data.folders[1].list.push($scope.store.place);
			}

			var newStore = {
				name : $scope.store.name,
				tags : [$scope.store.dish, $scope.store.place]
			};

			$rootScope.data.stores.push(newStore);

			$.ajax({
				url: '/Save',
				type: 'POST',
				data: JSON.stringify($rootScope.data),
				contentType: 'application/json',
				dataType: "json",
				success: function(json) {
					console.log(json);
					alert('success');
				},
				error: function() {
					alert('error');
				}
			});
		};
	}
]);

})();
function gglApiCallback(d) {
	var image = d.responseData.results[0].url;
	document.getElementsByClassName("gglImage")[0].src = image;
}

