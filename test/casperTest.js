/*
var casper = require('casper').create();

casper.start('http://192.168.5.28:5000/', function(){
	this.echo(this.getTitle());		
});

casper.then(function() {
	test.assertTitle('Want');
});

casper.run();
*/


var x = require('casper').selectXPath;
casper.options.viewportSize = {width: 1280, height: 939};
casper.on('page.error', function(msg, trace) {
   this.echo('Error: ' + msg, 'ERROR');
   for(var i=0; i<trace.length; i++) {
       var step = trace[i];
       this.echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR');
   }
});
casper.test.begin('Resurrectio test', function(test) {
   casper.start('http://192.168.5.28:5000/');
   casper.then(function() {
       test.assertTitle('Want');
   });

   casper.run(function() {test.done();});
});
