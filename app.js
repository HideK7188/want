var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();

app.use(bodyParser());
app.use(bodyParser.json());

app.post('/Save', function(req, res) {
	fs.writeFile('data/save.json', JSON.stringify(req.body, null, 4), function(err) {
		if(err) {
			console.log(err);
			res.json({status: err});
		} else {
			console.log("JSON saved")
			res.json({status:'success'});
		}
	});
});

// index.html, そのほかcss, jsなどをルーティングなしで利用する
app.use(express.static(__dirname));

module.exports = app;

if(!module.parent) {
	app.listen(5000, function() {
		// gruntのtaskを進行させるために以下の出力が必要
		console.log('server start');
	});
}
