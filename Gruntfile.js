'use strict';

module.exports = function(grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		express: {
			options: {
			},
			server: {
				options: {
					script: 'app.js'	
				}
			}
		},

		compass: {
			dist: {
				options: {
					sassDir: 'scss', 
					cssDir: 'css'
				}
			}
		},

		// for grunt-mocha(with phontomjs)
		/*
		mocha: {
			test: {
				options: {
					reporter: 'Nyan'
				},
				src: ['test/*.js']
			}
		},
		*/

		// for grunt-mocha-test
		mochaTest: {
			test: {
				options: {
					reporter: 'nyan'
				},
				src:['test/test.js']
			}
		},

		casper: {
			myTask: {
				options: {
					test: true
				},
				src: ['test/casperTest.js']
			}
		},

		jshint: {
			files: [
				'js/script.js',
				'data/*.json'
			],
			options: {
				jshintrc: '.jshintrc'
			}
		},

		watch: {
			express: {
				files: ['app.js'],
				tasks: ['express:server'],
				options: {
					spawn: false,
//					livereload: true
				}
			},
			html: {
				files: '**/*.html',
				tasks: [],
				options: {
//					livereload: true
				}
			},
			sass: {
				files: 'scss/*.scss',
				tasks: ['compass'],
				options: {
//					livereload: false
				}
			},
			css: {
				files: '**/*.css',
				tasks: [],
				options: {
//					livereload: true
				}
			},
			scripts: {
				files: 'js/*.js',
				tasks: ['jshint'],
				options: {
//					livereload: true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-express-server');
//	grunt.loadNpmTasks('grunt-mocha');
	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.loadNpmTasks('grunt-casper');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-jshint');

	grunt.registerTask('allTask', ['express:server', 'compass', 'mochaTest', 'casper', 'watch', 'jshint']);
	grunt.registerTask('default', 'allTask');
};
